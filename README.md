# Proyecto Guia de TV
Desarrollo de aplicacion para la evaluacion HITSS

Se construyo con las caracteristicas:
* Desarrollado en Kotlin, con Android Studio 3.1 y SDK 32.
* Arquitectura basada en la  ***Guía de arquitectura de apps*** de Android jetpack.
* Patron de diseño **MVVM** en Capa de UI, utilizando data binding, y viewmodels con livedata para la actualizacion de la vista.
* Se implemento **Repository pattern** en capa de datos, manejando repositorios abstractos que se usan en la capa de dominio ajeno a su implementacion.
