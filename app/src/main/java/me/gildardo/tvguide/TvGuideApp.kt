package me.gildardo.tvguide

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TvGuideApp : Application()