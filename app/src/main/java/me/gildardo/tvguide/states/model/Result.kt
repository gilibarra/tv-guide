package me.gildardo.tvguide.states.model

import android.content.Context
import me.gildardo.domain.model.ErrorCode
import me.gildardo.tvguide.R

sealed class Result<T> {
    class Loading<T>() : Result<T>()
    class Success<T>(val result: T) : Result<T>()
    class Error<T>(
        val code: Code,
        val errorDetail: String
    ) : Result<T>()
}

sealed class Code{
    object SERVER_ERROR: Code()
    object LOCAL_ERROR: Code()
}

fun Int.toCode(): Code = when(this){
    -1 -> Code.LOCAL_ERROR
    else -> Code.SERVER_ERROR
}

fun Code.getLabel(context: Context): String = when (this) {
    Code.SERVER_ERROR -> context.getString(R.string.app_name)
    Code.LOCAL_ERROR -> context.getString(R.string.check_your_internet_connection)
}

fun ErrorCode.toUi(): Code = when(this){
    ErrorCode.SERVER_ERROR -> Code.SERVER_ERROR
}
