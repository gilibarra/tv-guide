package me.gildardo.tvguide.ui.model

import android.os.Parcelable
import android.text.Html
import android.text.Spanned
import kotlinx.parcelize.Parcelize
import me.gildardo.domain.model.Show as ShowDomain
import me.gildardo.domain.model.Cast as CastDomain

@Parcelize
data class Show(
    val id: Int,
    val name: String,
    val channel: String,
    val rating: Double,
    val time: String,
    val days: List<String>,
    val image: String?,
    val link: String?,
    val summary: String?,
    val genres: List<String>?,
    val cast: List<Cast>?
): Parcelable {
    fun getRatingText() : String = "rating: $rating"
    fun getSummaryHTML(): Spanned? = Html.fromHtml(summary)
    fun getGenresString(): String = genres?.let { if (genres.isEmpty()) null else genres.joinToString(", ") } ?: "Without assigned gender"
    fun getScheduleString(): String = time + " | " + (days?.joinToString(", ") ?: "")
}
fun ShowDomain.toUi(): Show = Show(
    id = id,
    name = name,
    channel = channel,
    rating = rating,
    time = time,
    days = days,
    image = image,
    link = link,
    summary = summary,
    genres = genres,
    cast = cast?.map {
        it.toUi()
    }
)

@Parcelize
data class Cast(
    val name: String,
    val image: String?,
): Parcelable
fun CastDomain.toUi(): Cast = Cast(
    name = name,
    image = image
)