package me.gildardo.tvguide.states.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import me.gildardo.domain.model.Response
import me.gildardo.domain.usecases.DateUseCase
import me.gildardo.domain.usecases.ScheduleListUseCase
import me.gildardo.domain.usecases.SearchShowsUseCase
import me.gildardo.tvguide.ui.model.Show
import me.gildardo.tvguide.states.model.Result
import me.gildardo.tvguide.states.model.toCode
import me.gildardo.tvguide.ui.model.toUi
import javax.inject.Inject

@HiltViewModel
class ScheduleViewModel @Inject constructor(
    private val scheduleListUseCase: ScheduleListUseCase,
    private val searchShowsUseCase: SearchShowsUseCase,
    private val dateUseCase: DateUseCase
) : ViewModel() {

    private val _scheduleList = MutableLiveData<Result<List<Show>>>()
    val scheduleList: LiveData<Result<List<Show>>> get() = _scheduleList

    private val _searchShows = MutableLiveData<Result<List<Show>>>()
    val searchShows: LiveData<Result<List<Show>>> get() = _searchShows

    private val _search = MutableLiveData("")

    fun onCreate() = viewModelScope.launch {
        if (scheduleList.value != null) return@launch
        if (scheduleList.value is Result.Success) return@launch
        getScheduleList()
    }

    fun getDayCompleteDateFormat() : String = dateUseCase().toList()[1]

    fun getScheduleList() = viewModelScope.launch {
        _scheduleList.postValue(Result.Loading())
        val (date,_) = dateUseCase()
        when(val result = scheduleListUseCase(date)){
            is Response.Error -> _scheduleList.postValue(Result.Error(result.code.toCode(), result.error))
            is Response.Success -> _scheduleList.postValue(Result.Success(result.result.map { it.toUi() }))
        }
    }

    fun searchShows(search: String) = viewModelScope.launch {
        // When the search isn't valid
        if (search.isEmpty() || search.isBlank()){
            _search.postValue("")
            // Show previous data if exist, else load normal data
            if (scheduleList.value is Result.Success){
                _scheduleList.postValue(scheduleList.value)
            } else {
                getScheduleList()
            }
            return@launch
        }

        _search.postValue(search)
        _searchShows.postValue(Result.Loading())
        when(val result = searchShowsUseCase(search)){
            is Response.Error -> _searchShows.postValue(Result.Error(result.code.toCode(), result.error))
            is Response.Success -> {
                if (_search.value!!.isNotEmpty()){ // No show results when search is empty
                    _searchShows.postValue(Result.Success(result.result.map { it.toUi() }))
                }
            }
        }

    }

}