package me.gildardo.tvguide.states.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import me.gildardo.domain.model.Response
import me.gildardo.domain.usecases.ShowDetailsUseCase
import me.gildardo.domain.usecases.CastLoadUseCase
import me.gildardo.tvguide.states.model.Result
import me.gildardo.tvguide.states.model.toCode
import me.gildardo.tvguide.ui.model.Show
import me.gildardo.tvguide.ui.model.Cast
import me.gildardo.tvguide.ui.model.toUi
import javax.inject.Inject

@HiltViewModel
class ShowDetailViewModel  @Inject constructor(
    private val showDetailsUseCase: ShowDetailsUseCase,
    private val castLoadUseCase: CastLoadUseCase
) : ViewModel() {

    private val _show = MutableLiveData<Result<Show>>()
    val show: LiveData<Result<Show>> get() = _show

    private val _castList = MutableLiveData<Result<List<Cast>>>()
    val castList: LiveData<Result<List<Cast>>> get() = _castList

    fun onCreate(id: Int) = viewModelScope.launch {
        if (show.value != null) return@launch
        if (show.value is Result.Success) return@launch
        getShow(id)
    }

    fun getShow(id: Int) = viewModelScope.launch {
        _show.postValue(Result.Loading())
        when(val result = showDetailsUseCase(id)){
            is Response.Error -> _show.postValue(Result.Error(result.code.toCode(), result.error))
            is Response.Success -> _show.postValue(Result.Success(result.result.toUi()))
        }
    }
    fun getCast(id: Int) = viewModelScope.launch {
        when(val result = castLoadUseCase(id)){
            is Response.Error -> _castList.postValue(Result.Error(result.code.toCode(), result.error))
            is Response.Success -> _castList.postValue(Result.Success(result.result.map {it.toUi()}))
        }
    }

}