package me.gildardo.tvguide.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.gildardo.tvguide.databinding.ItemCastBinding
import me.gildardo.tvguide.ui.model.Cast

class CastAdapter : RecyclerView.Adapter<CastAdapter.ViewHolder>() {

    private val casts = arrayListOf<Cast>()

    fun setList(casts: List<Cast>) {
        this.casts.apply {
            clear()
            addAll(casts)
        }
        notifyDataSetChanged()
    }

    fun addItems(casts: List<Cast>) {
        val positionStart = this.casts.size
        this.casts.addAll(casts)
        notifyItemRangeInserted(positionStart, casts.size)
    }

    fun clearList() {
        this.casts.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        ItemCastBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(casts[position], position)

    override fun getItemCount(): Int = casts.size
    inner class ViewHolder(
        private val binding: ItemCastBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Cast, position: Int) {
            binding.castItem = item
        }
    }
}