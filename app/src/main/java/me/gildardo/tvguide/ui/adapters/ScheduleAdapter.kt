package me.gildardo.tvguide.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.gildardo.tvguide.databinding.ItemScheduleShowBinding
import me.gildardo.tvguide.ui.model.Show

class ScheduleAdapter(
    private val onClickShow: (show: Show, position: Int) -> Unit
) : RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {

    private val shows = arrayListOf<Show>()

    fun setList(shows: List<Show>) {
        this.shows.apply {
            clear()
            addAll(shows)
        }
        notifyDataSetChanged()
    }

    fun addItems(shows: List<Show>) {
        val positionStart = this.shows.size
        this.shows.addAll(shows)
        notifyItemRangeInserted(positionStart, shows.size)
    }

    fun clearList() {
        this.shows.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        ItemScheduleShowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(shows[position], position)

    override fun getItemCount(): Int = shows.size
    inner class ViewHolder(
        private val binding: ItemScheduleShowBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Show, position: Int) {
            binding.showItem = item
            binding.itemSchedule.setOnClickListener { onClickShow(item, position) }
        }
    }
}