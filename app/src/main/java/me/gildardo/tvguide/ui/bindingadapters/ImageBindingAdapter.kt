package me.gildardo.tvguide.ui.bindingadapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load
import me.gildardo.tvguide.R

@BindingAdapter("drawables_res")
fun loadDrawableRes(iv: ImageView, drawableRes: Int?) {
    if (drawableRes == null) {
        iv.setImageDrawable(null)
        return
    }
    iv.setImageResource(drawableRes)
}

@BindingAdapter("thumbnail")
fun loadThumbnail(iv: ImageView, thumbnail: String?) {
    if (thumbnail == null) {
        iv.setImageResource(R.drawable.ic_baseline_image_24)
        return
    }

    iv.load(thumbnail) {
        crossfade(true)
        placeholder(R.drawable.ic_baseline_image_24)
    }
}