package me.gildardo.tvguide.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import me.gildardo.tvguide.states.model.Result
import dagger.hilt.android.AndroidEntryPoint
import me.gildardo.tvguide.R
import me.gildardo.tvguide.databinding.FragmentScheduleBinding
import me.gildardo.tvguide.states.model.Code
import me.gildardo.tvguide.ui.adapters.ScheduleAdapter
import me.gildardo.tvguide.states.viewmodels.ScheduleViewModel

@AndroidEntryPoint
class ScheduleFragment() : Fragment() {

    private var _binding: FragmentScheduleBinding? = null
    private val binding: FragmentScheduleBinding get() = _binding!!
    private var openSearch: Boolean = false
    private val scheduleViewModel: ScheduleViewModel by viewModels()
    private lateinit var scheduleAdapter: ScheduleAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentScheduleBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSearchsListeners()
        initAdapter()
        observeResults()
        binding.txtDayComplete!!.text = scheduleViewModel.getDayCompleteDateFormat()
        scheduleViewModel.onCreate()
    }

    private fun setSearchsListeners() {
        binding.inputSearch!!.visibility = View.INVISIBLE
        binding.btnSearch!!.setOnClickListener {
            openSearch = true
            binding.inputSearch.text?.clear()
            setSearchToolbar()
        }

        binding.inputSearch.addTextChangedListener { editable ->
            scheduleViewModel.searchShows(editable.toString())
        }

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(openSearch) {
                    openSearch = false
                    setSearchToolbar()
                    scheduleViewModel.getScheduleList()
                } else {
                    isEnabled = false
                    activity?.onBackPressed()
                }
            }
        })
    }

    private fun setSearchToolbar(){
        binding.viewDate!!.visibility = if (openSearch) View.INVISIBLE else View.VISIBLE
        binding.inputSearch!!.visibility = if (openSearch) View.VISIBLE else View.INVISIBLE
    }

    private fun initAdapter() {
        scheduleAdapter = ScheduleAdapter() { show, _ ->
            openSearch = false
            val bundle = bundleOf(ShowDetailFragment.EXTRA_SHOW_ID to show)
            Navigation
                .findNavController(binding.root)
                .navigate(R.id.action_scheduleFragment_to_showDetailFragment, bundle)
        }
        binding.rvShows.adapter = scheduleAdapter
    }

    private fun observeResults() {
        scheduleViewModel.scheduleList.observe(viewLifecycleOwner){result ->
            isLoading(false)
            when(result) {
                is Result.Error -> showError(result.code, result.errorDetail)
                is Result.Loading -> isLoading(true)
                is Result.Success -> {
                    binding.noticeWithoutResults!!.visibility = View.GONE
                    binding.rvShows.visibility = View.VISIBLE
                    scheduleAdapter.setList(result.result)
                }
            }
        }
        scheduleViewModel.searchShows.observe(viewLifecycleOwner){result ->
            isLoading(false)
            when(result) {
                is Result.Error -> showError(result.code, result.errorDetail)
                is Result.Loading -> isLoading(true)
                is Result.Success -> {
                    if (result.result.isNotEmpty()){
                        binding.noticeWithoutResults!!.visibility = View.GONE
                        binding.rvShows.visibility = View.VISIBLE
                        scheduleAdapter.setList(result.result)
                    } else {
                        binding.noticeWithoutResults!!.visibility = View.VISIBLE
                        binding.rvShows.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun isLoading(isLoading: Boolean) {
        binding.progressIndicatorLoader.visibility = if (isLoading) View.VISIBLE else View.INVISIBLE
        binding.rvShows.visibility = if (!isLoading) View.VISIBLE else View.INVISIBLE
        if (!isLoading)
            binding.noticeWithoutResults!!.visibility = View.INVISIBLE
    }

    private fun showError(code: Code, errorDetail: String) {
        Log.e("Error consumo", errorDetail)
    }
}