package me.gildardo.tvguide.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import me.gildardo.tvguide.R
import me.gildardo.tvguide.databinding.FragmentShowDetailBinding
import me.gildardo.tvguide.states.model.Code
import me.gildardo.tvguide.states.model.Result
import me.gildardo.tvguide.states.viewmodels.ShowDetailViewModel
import me.gildardo.tvguide.ui.adapters.CastAdapter
import me.gildardo.tvguide.ui.model.Show


@AndroidEntryPoint
class ShowDetailFragment : Fragment() {

    companion object{
        const val EXTRA_SHOW_ID = "EXTRA_SHOW_ID"
    }

    private var _binding: FragmentShowDetailBinding? = null
    private val binding: FragmentShowDetailBinding get() = _binding!!
    private val showDetailViewModel: ShowDetailViewModel by viewModels()
    private lateinit var castAdapter: CastAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShowDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<Show>(EXTRA_SHOW_ID)?.let { show ->
            initAdapter()
            observeResults()
            showDetailViewModel.onCreate(show.id)
        } ?: showToast(getString(R.string.without_results))

    }

    private fun initAdapter() {
        castAdapter = CastAdapter()
        binding.rvCast!!.adapter = castAdapter
    }

    private fun observeResults() {
        showDetailViewModel.show.observe(viewLifecycleOwner){result ->
            when(result) {
                is Result.Loading -> isLoading(true)
                is Result.Error -> showError(result.code, result.errorDetail)
                is Result.Success -> {
                    binding.showItem = result.result
                    showDetailViewModel.getCast(result.result.id)

                    binding.btnLink.setOnClickListener {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(result.result.link)))
                    }
                }
            }
        }
        showDetailViewModel.castList.observe(viewLifecycleOwner){result ->
            isLoading(false)
            when(result) {
                is Result.Error -> showError(result.code, result.errorDetail)
                is Result.Loading -> isLoading(true)
                is Result.Success -> {
                    castAdapter.setList(result.result)
                    isLoading(false)
                }
            }
        }
    }

    private fun isLoading(isLoading: Boolean) {
        binding.progressIndicatorLoader.visibility = if (isLoading) View.VISIBLE else View.INVISIBLE
    }

    private fun showError(code: Code, errorDetail: String) {
        Log.e("Error consumo", errorDetail)
        showToast(errorDetail)
    }

    private fun showToast(message: String) =
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
}