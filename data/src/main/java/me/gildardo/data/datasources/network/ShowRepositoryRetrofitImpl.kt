package me.gildardo.data.datasources.network

import me.gildardo.data.datasources.network.response.toModel
import me.gildardo.data.repositories.ShowRepository
import me.gildardo.data.repositories.model.Cast
import me.gildardo.data.repositories.model.Response
import me.gildardo.data.repositories.model.Show
import javax.inject.Inject

class ShowRepositoryRetrofitImpl @Inject constructor(
    private val tvMazeApi: TvMazeApi
):  ShowRepository {

    override suspend fun getSchedule(date: String): Response<List<Show>> = try {
        val response = tvMazeApi.getSchedule(date = date)
        if (response.isSuccessful) {
            Response.Success(response.body()!!.map { it.toModel() })
        } else {
            Response.Error(response.code(), response.message())
        }
    } catch (e: Exception) {
        Response.Error(LOCAL_ERROR, e.toString())
    }

    override suspend fun getShows(query: String): Response<List<Show>> = try {
        val response = tvMazeApi.getShows(query)
        if (response.isSuccessful) {
            Response.Success(response.body()!!.map { it.toModel() })
        } else {
            Response.Error(response.code(), response.message())
        }
    } catch (e: Exception) {
        Response.Error(LOCAL_ERROR, e.toString())
    }

    override suspend fun getShow(id: Int): Response<Show> = try {
        val response = tvMazeApi.getShow(id)
        if (response.isSuccessful) {
            Response.Success(response.body()!!.toModel())
        } else {
            Response.Error(response.code(), response.message())
        }
    } catch (e: Exception) {
        Response.Error(LOCAL_ERROR, e.toString())
    }

    override suspend fun getCast(id: Int): Response<List<Cast>> = try {
        val response = tvMazeApi.getCast(id)
        if (response.isSuccessful) {
            Response.Success(response.body()!!.map { it.toModel() })
        } else {
            Response.Error(response.code(), response.message())
        }
    } catch (e: Exception) {
        Response.Error(LOCAL_ERROR, e.toString())
    }

}