package me.gildardo.data.datasources.network

import me.gildardo.data.datasources.network.response.CastResponse
import me.gildardo.data.datasources.network.response.ScheduleResponse
import me.gildardo.data.datasources.network.response.ShowSearchResponse
import me.gildardo.data.datasources.network.response.ShowResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvMazeApi {

    @GET("schedule")
    suspend fun getSchedule(
        @Query("country") country: String = "US",
        @Query("date") date: String
    ): Response<List<ScheduleResponse>>

    @GET("search/shows")
    suspend fun getShows(
        @Query("q") query: String
    ): Response<List<ShowSearchResponse>>

    @GET("shows/{id}")
    suspend fun getShow(
        @Path("id") id: Int
    ): Response<ShowResponse>

    @GET("shows/{id}/cast")
    suspend fun getCast(
        @Path("id") id: Int
    ): Response<List<CastResponse>>

}