package me.gildardo.data.datasources.network.response

import me.gildardo.data.repositories.model.Cast

data class CastResponse(
    val person: CastPerson
)

data class CastPerson(
    val name: String,
    val image: CastImage?
)

data class CastImage(
    val medium: String
)
fun CastResponse.toModel(): Cast = Cast(
    name = person.name,
    image =  person.image?.medium,
)