package me.gildardo.data.datasources.network.response

import me.gildardo.data.repositories.model.Show

data class ScheduleResponse(
    val show: ShowSchedule,
    val airdate: String,
    val airtime: String
)
data class ShowSchedule(
    val id: Int,
    val name: String,
    val network: channelData?,
    val webChannel: channelData?,
    val image: scheduleImage?
)
data class channelData(
    val name: String
)

data class scheduleImage(
    val medium: String
)

fun ScheduleResponse.toModel(): Show = Show(
    show.id,
    show.name,
    show.network?.name ?: show.webChannel?.name ?: "Not channel",
    0.0,
    "$airdate | $airtime",
    ArrayList<String>(),
    show.image?.medium ?: "",
    null,
    null,
    null,
    null
)