package me.gildardo.data.datasources.network.response

import me.gildardo.data.repositories.model.Show

data class ShowResponse(
    val id: Int,
    val name: String,
    val network: ChannelShow?,
    val webChannel: ChannelShow?,
    val image: ImageShow?,
    val schedule: ScheduleShow,
    val rating: RatingShow,
    val officialSite: String,
    val summary: String,
    val genres: List<String>
)
data class ChannelShow(
    val name: String
)

data class ImageShow(
    val medium: String
)

data class RatingShow(
    val average: Double
)

data class ScheduleShow(
    val time: String,
    val days: List<String>
)

fun ShowResponse.toModel(): Show = Show(
    id,
    name,
    network?.name ?: webChannel?.name ?: "Not channel",
    rating.average,
    schedule.time,
    schedule.days,
    image?.medium ?: "",
    officialSite,
    summary,
    genres,
    null
)