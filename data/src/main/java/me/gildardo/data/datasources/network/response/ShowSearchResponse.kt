package me.gildardo.data.datasources.network.response

import me.gildardo.data.repositories.model.Show

data class ShowSearchResponse(
    val show: ShowSearch
)
data class ShowSearch(
    val id: Int,
    val name: String,
    val network: ChannelSearch?,
    val webChannel: ChannelSearch?,
    val image: ImageSearch?,
    val schedule: Schedule
)
data class ChannelSearch(
    val name: String
)

data class ImageSearch(
    val medium: String
)

data class Schedule(
    val time: String,
    val days: List<String>
)

fun ShowSearchResponse.toModel(): Show = Show(
    show.id,
    show.name,
    show.network?.name ?: show.webChannel?.name ?: "Not channel",
    0.0,
    show.schedule.time + " | " + show.schedule.days[0],
    ArrayList<String>(),
    show.image?.medium ?: "",
    null,
    null,
    null,
    null
)
