package me.gildardo.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.gildardo.data.datasources.network.ShowRepositoryRetrofitImpl
import me.gildardo.data.datasources.network.TvMazeApi
import me.gildardo.data.repositories.ShowRepository
import javax.inject.Inject

@Module
@InstallIn(SingletonComponent::class)
class DataSourceModule {
    @Inject
    @Provides
    fun provideShowRepository(tvMazeApi: TvMazeApi): ShowRepository = ShowRepositoryRetrofitImpl(tvMazeApi)
}
