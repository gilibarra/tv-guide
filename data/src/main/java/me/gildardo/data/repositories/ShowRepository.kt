package me.gildardo.data.repositories

import me.gildardo.data.repositories.model.Cast
import me.gildardo.data.repositories.model.Response
import me.gildardo.data.repositories.model.Show

interface ShowRepository {
    suspend fun getSchedule(date: String): Response<List<Show>>
    suspend fun getShows(query: String): Response<List<Show>>
    suspend fun getShow(id: Int): Response<Show>
    suspend fun getCast(id: Int): Response<List<Cast>>
}