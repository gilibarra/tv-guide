package me.gildardo.data.repositories.model

sealed class Response<T> {
    class Success<T>(val result: T): Response<T>()
    class Error<T>(val code: Int, val error: String): Response<T>()
}