package me.gildardo.data.repositories.model

data class Show(
    val id: Int,
    val name: String,
    val channel: String,
    val rating: Double,
    val time: String,
    val days: List<String>,
    val image: String?,
    val link: String?,
    val summary: String?,
    val genres: List<String>?,
    val cast: List<Cast>?
)

data class Cast(
    val name: String,
    val image: String?,
)