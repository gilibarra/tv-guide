package me.gildardo.domain.model

sealed class Response<T> {
    class Success<T>(val result: T): Response<T>()
    class Error<T>(val code: Int, val error: String): Response<T>()
}

enum class ErrorCode{
    SERVER_ERROR
}