package me.gildardo.domain.model

import me.gildardo.data.repositories.model.Show as ShowData
import me.gildardo.data.repositories.model.Cast as CastData

data class Show(
    val id: Int,
    val name: String,
    val channel: String,
    val rating: Double,
    val time: String,
    val days: List<String>,
    val image: String?,
    val link: String?,
    val summary: String?,
    val genres: List<String>?,
    val cast: List<Cast>?
)


fun ShowData.toDomain(): Show = Show(
    id = id,
    name = name,
    channel = channel,
    rating = rating,
    time = time,
    days = days,
    image = image,
    link = link,
    summary = summary,
    genres = genres,
    cast = cast?.map {
        it.toDomain()
    }
)

data class Cast(
    val name: String,
    val image: String?,
)

fun CastData.toDomain(): Cast = Cast(
    name = name,
    image = image
)