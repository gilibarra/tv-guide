package me.gildardo.domain.usecases

import me.gildardo.data.repositories.ShowRepository
import me.gildardo.data.repositories.model.Response
import me.gildardo.domain.model.Response as ResponseDomain
import me.gildardo.domain.model.Cast
import me.gildardo.domain.model.toDomain
import javax.inject.Inject

class CastLoadUseCase @Inject constructor(
    private val showRepository: ShowRepository
) {
    suspend operator fun invoke(id: Int): ResponseDomain<List<Cast>> {
        return when (val response = showRepository.getCast(id)) {
            is Response.Error -> ResponseDomain.Error(response.code, response.error)
            is Response.Success -> ResponseDomain.Success(response.result.map { it.toDomain() })
        }
    }
}