package me.gildardo.domain.usecases

import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DateUseCase @Inject constructor() {
    operator fun invoke(): Pair<String,String> {
        val calendar = Calendar.getInstance()
        val dayDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val dayCompleteDateFormat = SimpleDateFormat("EEEE d 'de' MMMM yyyy", Locale("es", "ES"))
        return Pair(dayDateFormat.format(calendar.time).toString(), dayCompleteDateFormat.format(calendar.time).toString())
    }
}