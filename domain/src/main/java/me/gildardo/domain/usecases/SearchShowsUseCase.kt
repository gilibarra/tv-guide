package me.gildardo.domain.usecases

import me.gildardo.data.repositories.ShowRepository
import me.gildardo.data.repositories.model.Response
import me.gildardo.domain.model.Show
import me.gildardo.domain.model.toDomain
import me.gildardo.domain.model.Response as ResponseDomain
import javax.inject.Inject

class SearchShowsUseCase @Inject constructor(
    private val showRepository: ShowRepository
) {
    suspend operator fun invoke(query: String): ResponseDomain<List<Show>> {
        return when (val response = showRepository.getShows(query)) {
            is Response.Error -> ResponseDomain.Error(response.code, response.error)
            is Response.Success -> ResponseDomain.Success(response.result.map { it.toDomain() })
        }
    }
}