package me.gildardo.domain.usecases

import me.gildardo.data.repositories.ShowRepository
import me.gildardo.data.repositories.model.Response
import me.gildardo.domain.model.Response as ResponseDomain
import me.gildardo.domain.model.Show
import me.gildardo.domain.model.toDomain
import javax.inject.Inject

class ShowDetailsUseCase @Inject constructor(
    private val showRepository: ShowRepository
) {
    suspend operator fun invoke(id: Int): ResponseDomain<Show> {
        return when (val response = showRepository.getShow(id)) {
            is Response.Error -> ResponseDomain.Error(response.code, response.error)
            is Response.Success -> ResponseDomain.Success(response.result.toDomain())
        }
    }
}